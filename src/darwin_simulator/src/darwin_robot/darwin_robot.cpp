#include "webots/Robot.hpp"
#include "webots/Motor.hpp"
#include "webots/PositionSensor.hpp"
#include "rclcpp/rclcpp.hpp"

#include "darwin_simulator/srv/process_servos.hpp"

using ProcessServos = darwin_simulator::srv::ProcessServos;
using FutureReturnCode = rclcpp::executor::FutureReturnCode;

int main(int argc, char **argv)
{
  auto robot = new webots::Robot();
  int timestep = robot->getBasicTimeStep();

  std::map<int, webots::Motor *> motor_map;
  motor_map.emplace(1, robot->getMotor("ShoulderR"));
  motor_map.emplace(2, robot->getMotor("ShoulderL"));
  motor_map.emplace(3, robot->getMotor("ArmUpperR"));
  motor_map.emplace(4, robot->getMotor("ArmUpperL"));
  motor_map.emplace(5, robot->getMotor("ArmLowerR"));
  motor_map.emplace(6, robot->getMotor("ArmLowerL"));
  motor_map.emplace(7, robot->getMotor("PelvYR"));
  motor_map.emplace(8, robot->getMotor("PelvYL"));
  motor_map.emplace(9, robot->getMotor("PelvR"));
  motor_map.emplace(10, robot->getMotor("PelvL"));
  motor_map.emplace(11, robot->getMotor("LegUpperR"));
  motor_map.emplace(12, robot->getMotor("LegUpperL"));
  motor_map.emplace(13, robot->getMotor("LegLowerR"));
  motor_map.emplace(14, robot->getMotor("LegLowerL"));
  motor_map.emplace(15, robot->getMotor("AnkleR"));
  motor_map.emplace(16, robot->getMotor("AnkleL"));
  motor_map.emplace(17, robot->getMotor("FootR"));
  motor_map.emplace(18, robot->getMotor("FootL"));
  motor_map.emplace(19, robot->getMotor("Neck"));
  motor_map.emplace(20, robot->getMotor("Head"));

  std::map<int, webots::PositionSensor *> position_sensor_map;
  position_sensor_map.emplace(1, robot->getPositionSensor("ShoulderRS"));
  position_sensor_map.emplace(2, robot->getPositionSensor("ShoulderLS"));
  position_sensor_map.emplace(3, robot->getPositionSensor("ArmUpperRS"));
  position_sensor_map.emplace(4, robot->getPositionSensor("ArmUpperLS"));
  position_sensor_map.emplace(5, robot->getPositionSensor("ArmLowerRS"));
  position_sensor_map.emplace(6, robot->getPositionSensor("ArmLowerLS"));
  position_sensor_map.emplace(7, robot->getPositionSensor("PelvYRS"));
  position_sensor_map.emplace(8, robot->getPositionSensor("PelvYLS"));
  position_sensor_map.emplace(9, robot->getPositionSensor("PelvRS"));
  position_sensor_map.emplace(10, robot->getPositionSensor("PelvLS"));
  position_sensor_map.emplace(11, robot->getPositionSensor("LegUpperRS"));
  position_sensor_map.emplace(12, robot->getPositionSensor("LegUpperLS"));
  position_sensor_map.emplace(13, robot->getPositionSensor("LegLowerRS"));
  position_sensor_map.emplace(14, robot->getPositionSensor("LegLowerLS"));
  position_sensor_map.emplace(15, robot->getPositionSensor("AnkleRS"));
  position_sensor_map.emplace(16, robot->getPositionSensor("AnkleLS"));
  position_sensor_map.emplace(17, robot->getPositionSensor("FootRS"));
  position_sensor_map.emplace(18, robot->getPositionSensor("FootLS"));
  position_sensor_map.emplace(19, robot->getPositionSensor("NeckS"));
  position_sensor_map.emplace(20, robot->getPositionSensor("HeadS"));

  for (auto &position_sensor_keyval : position_sensor_map)
  {
    position_sensor_keyval.second->enable(timestep);
  }

  robot->step(timestep);

  rclcpp::init(argc, argv);

  auto node = rclcpp::Node::make_shared("darwin_robot");

  std::string client_name = robot->getName() + "/process_servos";
  auto client = node->create_client<ProcessServos>(client_name);

  RCLCPP_INFO(node->get_logger(), "starting robot controller");

  while (robot->step(timestep) != -1 && rclcpp::ok())
  {
    if (client->service_is_ready() == false)
      continue;

    auto request = std::make_shared<ProcessServos::Request>();
    for (auto &position_sensor_keyval : position_sensor_map)
    {
      request->ids.push_back(position_sensor_keyval.first);
      request->read_positions.push_back(position_sensor_keyval.second->getValue());
    }

    auto result_future = client->async_send_request(request);
    if (rclcpp::spin_until_future_complete(node, result_future) != FutureReturnCode::SUCCESS)
    {
      RCLCPP_ERROR(node->get_logger(), "service call failed");
      return 1;
    }

    auto result = result_future.get();
    for (int i = 0; i < request->ids.size(); i++)
    {
      auto id = request->ids[i];

      if (motor_map.find(id) != motor_map.end())
      {
        auto target_position = result->target_positions[i];
        motor_map[id]->setPosition(target_position);

        auto p_gain = result->p_gains[i] * 0.2;
        auto i_gain = result->i_gains[i] * 0.2;
        auto d_gain = result->d_gains[i] * 0.2;
        motor_map[id]->setControlPID(p_gain, i_gain, d_gain);
      }
    }
  }

  return 0;
}