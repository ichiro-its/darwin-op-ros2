#include "webots/Robot.hpp"
#include "webots/Accelerometer.hpp"
#include "webots/Gyro.hpp"
#include "rclcpp/rclcpp.hpp"

// #include "darwin_simulator/srv/process_imu.hpp"
#include "darwin_simulator/msg/stream_imu.hpp"

// using ProcessImu = darwin_simulator::srv::ProcessImu;
using StreamImu = darwin_simulator::msg::StreamImu;
using FutureReturnCode = rclcpp::executor::FutureReturnCode;

int main(int argc, char **argv)
{
  auto robot = std::make_shared<webots::Robot>();
  int timestep = robot->getBasicTimeStep();

  auto accelerometer = robot->getAccelerometer("Accelerometer");
  accelerometer->enable(timestep);

  auto gyro = robot->getGyro("Gyro");
  gyro->enable(timestep);

  robot->step(timestep);

  rclcpp::init(argc, argv);

  auto node = rclcpp::Node::make_shared("darwin_imu");

  // std::string client_name = robot->getName() + "/process_imu";
  // auto client = node->create_client<ProcessImu>(client_name);

  std::string publisher_name = robot->getName() + "/stream_imu";
  auto publisher = node->create_publisher<StreamImu>(publisher_name, 10);

  RCLCPP_INFO(node->get_logger(), "starting imu controller");

  while (robot->step(timestep) != -1 && rclcpp::ok())
  {
    // if (client->service_is_ready() == false)
    //   continue;

    // auto request = std::make_shared<ProcessImu::Request>();
    auto message = StreamImu();

    auto accelerometer_value = accelerometer->getValues();

    // request->accelerometer_x = accelerometer_value[0];
    // request->accelerometer_y = accelerometer_value[1];
    // request->accelerometer_z = accelerometer_value[2];

    message.accelerometer_x = accelerometer_value[0];
    message.accelerometer_y = accelerometer_value[1];
    message.accelerometer_z = accelerometer_value[2];

    auto gyro_value = gyro->getValues();

    // request->gyroscope_x = gyro_value[0];
    // request->gyroscope_y = gyro_value[1];
    // request->gyroscope_z = gyro_value[2];

    message.gyroscope_x = gyro_value[0];
    message.gyroscope_y = gyro_value[1];
    message.gyroscope_z = gyro_value[2];

    // auto result_future = client->async_send_request(request);
    // if (rclcpp::spin_until_future_complete(node, result_future) != FutureReturnCode::SUCCESS)
    // {
    //   RCLCPP_ERROR(node->get_logger(), "service call failed");
    //   return 1;
    // }

    // auto result = result_future.get();

    // doing nothing with the result

    publisher->publish(message);
  }

  return 0;
}