#include <cstdlib>
#include <iostream>
#include <sstream>

#include "rclcpp/rclcpp.hpp"
#include "ament_index_cpp/get_package_share_directory.hpp"

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);

  auto world_file = ament_index_cpp::get_package_share_directory("darwin_simulator");
  world_file += "/worlds/simulator.wbt";

  std::cout << "running webots in " << world_file << std::endl;

  std::stringstream ss;
  ss << "webots " << world_file;

  std::system(ss.str().c_str());

  return 0;
}