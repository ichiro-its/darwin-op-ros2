#include "webots/Robot.hpp"
#include "webots/Camera.hpp"
#include "rclcpp/rclcpp.hpp"

// #include "darwin_simulator/srv/process_camera.hpp"
#include "darwin_simulator/msg/stream_camera.hpp"

// using ProcessCamera = darwin_simulator::srv::ProcessCamera;
using StreamCamera = darwin_simulator::msg::StreamCamera;
using FutureReturnCode = rclcpp::executor::FutureReturnCode;

int main(int argc, char **argv)
{
  auto robot = std::make_shared<webots::Robot>();
  int timestep = robot->getBasicTimeStep() * 4;

  auto camera = robot->getCamera("Camera");
  camera->enable(timestep);

  robot->step(timestep);

  rclcpp::init(argc, argv);

  auto node = rclcpp::Node::make_shared("darwin_camera");

  // std::string client_name = robot->getName() + "/process_camera";
  // auto client = node->create_client<ProcessCamera>(client_name);

  std::string publisher_name = robot->getName() + "/stream_camera";
  auto publisher = node->create_publisher<StreamCamera>(publisher_name, 10);

  RCLCPP_INFO(node->get_logger(), "starting camera controller");

  while (robot->step(timestep) != -1 && rclcpp::ok())
  {
    // if (client->service_is_ready() == false)
    //   continue;

    // auto request = std::make_shared<ProcessCamera::Request>();
    auto message = StreamCamera();

    // request->width = camera->getWidth();
    // request->height = camera->getHeight();

    message.width = camera->getWidth();
    message.height = camera->getHeight();

    int camera_data_size = camera->getWidth() * camera->getHeight() * 4;
    const unsigned char *camera_data = camera->getImage();
    // std::copy(&camera_data[0], &camera_data[camera_data_size], std::back_inserter(request->data));
    std::copy(&camera_data[0], &camera_data[camera_data_size], std::back_inserter(message.data));

    // auto result_future = client->async_send_request(request);
    // if (rclcpp::spin_until_future_complete(node, result_future) != FutureReturnCode::SUCCESS)
    // {
    //   RCLCPP_ERROR(node->get_logger(), "service call failed");
    //   return 1;
    // }

    // auto result = result_future.get();

    // doing nothing with the result

    publisher->publish(message);
  }

  return 0;
}