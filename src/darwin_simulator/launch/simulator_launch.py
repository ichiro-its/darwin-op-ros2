import os
import launch
import launch_ros

from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

  default_world_file = os.path.join(
    get_package_share_directory('darwin_simulator'),
    'worlds', 'simulator.wbt'
  )

  world_file = launch.substitutions.LaunchConfiguration('world_file', default = default_world_file)

  webots = launch_ros.actions.Node(
    package = 'darwin_simulator',
    node_executable = 'darwin_simulator_node',
    parameters = [{'world_file': world_file}],
    output = {
      'stdout': 'screen',
      'stderr': 'screen',
    },
    emulate_tty = True
  )

  return launch.LaunchDescription([
    webots,
    launch.actions.RegisterEventHandler(
      event_handler = launch.event_handlers.OnProcessExit(
        target_action = webots,
        on_exit = [launch.actions.EmitEvent(event = launch.events.Shutdown())],
      )
    )
  ])