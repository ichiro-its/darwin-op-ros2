/*
 *   BallTracker.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _BALL_TRACKER_H_
#define _BALL_TRACKER_H_

#include <string.h>

#include "darwin_math/point.h"
#include "darwin_minini/minini.h"

namespace Robot
{
	class BallTracker
	{
	private:
		int NoBallCount;
		static const int NoBallMaxCount = 15;

	public:
        Point2D     ball_position;

		BallTracker();
		~BallTracker();

		void Process(Point2D pos);
	};
}

#endif
