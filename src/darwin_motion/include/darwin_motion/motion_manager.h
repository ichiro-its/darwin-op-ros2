/*
 *   MotionManager.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _MOTION_MANGER_H_
#define _MOTION_MANGER_H_

#include <list>
#include <fstream>
#include <iostream>
#include "darwin_motion/motion_status.h"
#include "darwin_motion/motion_module.h"
#include "darwin_motion/cm730.h"
#include "darwin_minini/minini.h"

#include "rclcpp/rclcpp.hpp"
#include "darwin_simulator/srv/process_servos.hpp"
#include "darwin_simulator/srv/process_imu.hpp"
#include "darwin_simulator/msg/stream_imu.hpp"

#define OFFSET_SECTION "Offset"
#define INVALID_VALUE   -1024.0

namespace Robot
{
	class MotionManager : public rclcpp::Node
	{
	private:

		static std::shared_ptr<MotionManager> m_UniqueInstance;
		std::list<MotionModule*> m_Modules;

		// CM730 *m_CM730;

		bool m_ProcessEnable;
		bool m_Enabled;
		int m_FBGyroCenter;
		int m_RLGyroCenter;
		int m_CalibrationStatus;

		bool m_IsRunning;
		bool m_IsThreadRunning;
		bool m_IsLogging;

		std::ofstream m_LogFileStream;

    using ProcessServos = darwin_simulator::srv::ProcessServos;
    using ProcessImu = darwin_simulator::srv::ProcessImu;
    using StreamImu = darwin_simulator::msg::StreamImu;

    rclcpp::Service<ProcessServos>::SharedPtr m_ProcessServosServer;
    rclcpp::Service<ProcessImu>::SharedPtr m_ProcessImuServer;
    rclcpp::Subscription<StreamImu>::SharedPtr m_StreamImuSubscription;

	public:

		bool DEBUG_PRINT;
    int m_Offset[JointData::NUMBER_OF_JOINTS];

    MotionManager();
		~MotionManager();

		static std::shared_ptr<MotionManager> GetInstance();

		bool Initialize(CM730 *cm730);
		bool Reinitialize();

    void Process();

		void SetEnable(bool enable);
		bool GetEnable() { return m_Enabled; }
		void AddModule(MotionModule *module);
		void RemoveModule(MotionModule *module);

		void ResetGyroCalibration() { m_CalibrationStatus = 0; m_FBGyroCenter = 512; m_RLGyroCenter = 512; }
		int GetCalibrationStatus() { return m_CalibrationStatus; }
		void SetJointDisable(int index);

		void StartLogging();
		void StopLogging();

    void LoadINISettings(minIni* ini);
    void LoadINISettings(minIni* ini, const std::string &section);
    void SaveINISettings(minIni* ini);
    void SaveINISettings(minIni* ini, const std::string &section);

    static void ProcessServosCallback(const ProcessServos::Request::SharedPtr request,
      ProcessServos::Response::SharedPtr response);
    static void ProcessImuCallback(const ProcessImu::Request::SharedPtr request,
      ProcessImu::Response::SharedPtr response);
    static void StreamImuCallback(const StreamImu::SharedPtr message);
	};
}

#endif
